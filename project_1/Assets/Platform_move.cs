﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_Move : MonoBehaviour
{

    public bool MoveX;
    public bool MoveY;
    public bool MoveZ;
    public float MoveStepX;
    public float MoveStepY;
    public float MoveStepZ;
    public float NapryamX, NapryamY, NapryamZ = 0;
    public float OffSetX, OffSetY, OffSetZ = 0;
    public float MaxOffSetX, MaxOffSetY, MaxOffSetZ = 0;
    Transform StartPosition;
    Transform TargetPosition;

    void Start()
    {
        StartPosition = this.gameObject.transform;

    }

    // Update is called once per frame
    void Update() {
        if (OffSetX > MaxOffSetX)
            NapryamX = -1;
        if (OffSetX < 0)
            NapryamX = 1;

        OffSetX = OffSetX + (MoveStepX * NapryamX);
        print(OffSetX);
        print(MoveStepX);

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{

    int BallJumpPower = 500000;
    int BallMovePover = 50;
    Rigidbody Ball;
    bool IsGrounded;

    void Start()
    {
        Ball = this.gameObject.GetComponent<Rigidbody>();
        Ball.mass = 10;

    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded)
            Ball.AddForce(Vector3.up * BallJumpPower);

        if (Input.GetKey(KeyCode.W))
            Ball.AddForce(Vector3.forward * BallJumpPower);

        if (Input.GetKey(KeyCode.A))
            Ball.AddForce(Vector3.left * BallJumpPower);

        if (Input.GetKey(KeyCode.D))
            Ball.AddForce(Vector3.right * BallJumpPower);

        if (Input.GetKey(KeyCode.S))
            Ball.AddForce(Vector3.back * BallJumpPower);
    }
    private void OnCollisionEnter(Collision collision)
    {
        IsGrounded = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        IsGrounded = false;
    }



}